﻿using System.Collections.Generic;

using Godot;




namespace Factorial.Core.Iteration
{

	/// <summary>
	/// Area contains utility functions for iterating 2D space.
	/// </summary>
	public sealed class Area
	{
		/// <summary>
		/// Rect will enumerate each points from <paramref name="from"/>
		/// to <paramref name="to"/> with a step of <paramref name="step"/>.
		/// </summary>
		/// <param name="from">Starting point.</param>
		/// <param name="to">Ending point.</param>
		/// <param name="step">Step size.</param>
		/// <returns>Enumerable</returns>
		public static IEnumerable<Vector2> Rect( Vector2 from, Vector2 to, Vector2 step )
		{
			for( float x = from.x; x < to.x; x += step.x )
				for( float y = from.y; y < to.y; y += step.y )
					yield return new Vector2( x, y );
		}

		/// <summary>
		/// Rect will enumerate each points from <paramref name="from"/>
		/// to <paramref name="to"/> with a step of <c>new Vector2( 1, 1 )</c>
		/// <param name="from">Starting point.</param>
		/// <param name="to">Ending point.</param>
		/// <returns>Enumerable</returns>
		public static IEnumerable<Vector2> Rect( Vector2 from, Vector2 to )
		{
			return Rect( from, to, new Vector2( 1, 1 ) );
		}




		/// <summary>
		/// RectSize will enumerate each points inside of the given rectangle.
		/// <paramref name="topleft"/> is the topleft hand corner of the rectangle
		/// and <paramref name="size"/> is its size. <paramref name="step"/> is 
		/// the step between each points.
		/// </summary>
		/// <param name="topleft">Position of the rectangle's topleft hand corner</param>
		/// <param name="size">Size of the rectangle.</param>
		/// <param name="step"></param>
		/// <returns>Enumerable</returns>
		public static IEnumerable<Vector2> RectSize( Vector2 topleft, Vector2 size, Vector2 step )
		{
			return Rect( topleft, topleft + size, step );
		}

		/// <summary>
		/// RectSize will enumerate each points inside of the given rectangle.
		/// <paramref name="topleft"/> is the topleft hand corner of the rectangle
		/// and <paramref name="size"/> is its size. The step size is 
		/// <c>new Vector2(1, 1)</c>.
		/// </summary>
		/// <param name="topleft">Position of the rectangle's topleft hand corner</param>
		/// <param name="size">Size of the rectangle.</param>
		/// <returns>Enumerable</returns>
		public static IEnumerable<Vector2> RectSize( Vector2 topleft, Vector2 size )
		{
			return Rect( topleft, topleft + size );
		}

	}

}
