﻿using Godot;




namespace Factorial.Core.World
{
	/// <summary>
	/// Entity is the base class for all entity going 
	/// into a Grid object.
	/// </summary>
	public abstract class Entity : Node2D
	{

		#region EXPORTS

		/// <summary>
		/// Unique identifier of the entity. 
		/// </summary>
		[Export]
		public string Identifier;




		/// <summary>
		/// Width of the entity, in grid cell.
		/// </summary>
		[Export(PropertyHint.Range, "0,16")]
		public int Width = 1;
		/// <summary>
		/// Height of the entity, in grid cell.
		/// </summary>
		[Export(PropertyHint.Range, "0-16")]
		public int Height = 1;

		#endregion

		#region FIELDS

		/// <summary>
		/// Size of the entity, in grid coordinate.
		/// </summary>
		public Vector2 Size => new Vector2( Width, Height );




		/// <summary>
		/// grid is the grid in which the entity lives.
		/// </summary>
		protected Grid grid { get; }

		#endregion

		#region ACTUATORS

		/// <summary>
		/// OnNeighbourUpdated is called whenever an adhjacent cell
		/// is remove/set within the grid.
		/// </summary>
		public virtual void OnNeighbourUpdated( ) { }

		#endregion

	}
}
