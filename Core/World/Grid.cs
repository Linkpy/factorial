﻿using Sys = System;
using System.Collections.Generic;

using Godot;




namespace Factorial.Core.World
{

	/// <summary>
	/// Direction represents a 2D space direction.
	/// </summary>
	public enum Direction
	{
		/// <summary>
		/// Up represents the Y- direction.
		/// </summary>
		Up,
		/// <summary>
		/// Right represents the X+ direction.
		/// </summary>
		Right,
		/// <summary>
		/// Down represents the Y+ direction.
		/// </summary>
		Down,
		/// <summary>
		/// Left represents the X- direction.
		/// </summary>
		Left
	}




	/// <summary>
	/// Grid is the class handling the position and storage of 
	/// entity within itself.
	/// </summary>
	public class Grid : Node2D
	{

		#region EXPORTS

		/// <summary>
		/// CellSize is the size of a grid cell.
		/// </summary>
		[Export]
		public Vector2 CellSize;

		#endregion

		#region SPACE

		/// <summary>
		/// GridToWorld converts a grid coordinate to world coordinate.
		/// </summary>
		/// <param name="p">Grid coordinate</param>
		/// <returns>World coordinate</returns>
		public Vector2 GridToWorld( Vector2 p )
		{
			return ToGlobal( p * CellSize );
		}

		/// <summary>
		/// WorldToGrid converts a world cooridnate to grid coordinate.
		/// </summary>
		/// <param name="p">World coordinate.</param>
		/// <returns>Grid coordinate.</returns>
		public Vector2 WorldToGrid( Vector2 p )
		{
			return ( ToLocal( p ) / CellSize ).Floor( );
		}

		/// <summary>
		/// GSizeToWSize converts a grid size to a world size.
		/// </summary>
		/// <param name="s">Grid size.</param>
		/// <returns>World size.</returns>
		public Vector2 GSizeToWSize( Vector2 s )
		{
			return ( s * CellSize ).Floor( );
		}

		/// <summary>
		/// WSizeToWSize converts a world size to grid size.
		/// </summary>
		/// <param name="s">World size.</param>
		/// <returns>Grid size.</returns>
		public Vector2 WSizeToGSize( Vector2 s )
		{
			return ( s / CellSize ).Ceil( );
		}

		/// <summary>
		/// Snap makes a world coordinate snaps to a cell topleft corner
		/// world coordinate.
		/// </summary>
		/// <param name="p">World coordinate.</param>
		/// <returns>Snapped world coordinate.</returns>
		public Vector2 Snap( Vector2 p )
		{
			return GridToWorld( WorldToGrid( p ) );
		}




		/// <summary>
		/// GridRelative returns the grid coordinate relative to the <paramref name="p"/>
		/// towards the direction <paramref name="d"/>.
		/// </summary>
		/// <param name="p">Grid coordinate.</param>
		/// <param name="d">Direction.</param>
		/// <returns>Grid coordinate relative to <paramref name="p"/>.</returns>
		public Vector2 GridRelative( Vector2 p, Direction d )
		{
			switch( d )
			{
				case Direction.Up:
					return p + new Vector2(  0, -1 );
				case Direction.Right:
					return p + new Vector2(  1,  0 );
				case Direction.Down:
					return p + new Vector2(  0,  1 );
				case Direction.Left:
					return p + new Vector2( -1,  0 );
				default:
					throw new Sys.ArgumentOutOfRangeException( "o", "invalid direction" );
			}
		}

		/// <summary>
		/// WorldRelative returns the world coordinate relative to the <paramref name="p"/>
		/// towards the direction <paramref name="d"/>.
		/// </summary>
		/// <param name="p">World coordinate.</param>
		/// <param name="d">Direction.</param>
		/// <returns>World coordinate relative to <paramref name="p"/>.</returns>
		public Vector2 WorldRelative( Vector2 p, Direction d )
		{
			return GridToWorld( GridRelative( WorldToGrid( p ), d ) );
		}


		#endregion

		#region LOWLEVEL ACCESS

		/// <summary>
		/// HasAt checks if an entity is in the given
		/// grid coordinate.
		/// </summary>
		/// <param name="p">Grid coordinate.</param>
		/// <returns>True if an entity exists there.</returns>
		public bool HasAt( Vector2 p )
		{
			return entities.ContainsKey( p );
		}

		/// <summary>
		/// GetAt returns the entity at the given grid coordinate.
		/// If no entity exists, return null.
		/// </summary>
		/// <param name="p">Grid coordinate.</param>
		/// <returns>The entity there, or null.</returns>
		public Entity GetAt( Vector2 p )
		{
			if( !HasAt( p ) )
				return null;

			return entities[ p.Floor( ) ];
		}

		/// <summary>
		/// SetAt defines the entity at the given grid coordinate.
		/// Override if an entity is already there.
		/// </summary>
		/// <param name="p">Grid coordinate.</param>
		/// <param name="e">Entity to set.</param>
		public void SetAt( Vector2 p, Entity e )
		{
			entities[ p.Floor( ) ] = e;
		}

		/// <summary>
		/// EraseAt removes the entity at the given grid cooridnate.
		/// Does nothing if no entity is there.
		/// </summary>
		/// <param name="p">Grid cooridnate.</param>
		public void EraseAt( Vector2 p )
		{
			if( HasAt( p ) )
				entities.Remove( p.Floor( ) );
		}

		#endregion

		#region LOWLEVEL RELATIVE ACCESS

		/// <summary>
		/// HasAt checks if an entity is in the given
		/// grid coordinate.
		/// </summary>
		/// <param name="p">Grid coordinate.</param>
		/// <param name="d">Direction.</param>
		/// <returns>True if an entity exists there.</returns>
		public bool HasAt( Vector2 p, Direction d )
		{
			return HasAt( GridRelative( p, d ) );
		}

		/// <summary>
		/// GetAt returns the entity at the given grid coordinate.
		/// If no entity exists, return null.
		/// </summary>
		/// <param name="p">Grid coordinate.</param>
		/// <param name="d">Direction.</param>
		/// <returns>The entity there, or null.</returns>
		public Entity GetAt( Vector2 p, Direction d )
		{
			return GetAt( GridRelative( p, d ) );
		}

		/// <summary>
		/// SetAt defines the entity at the given grid coordinate.
		/// Override if an entity is already there.
		/// </summary>
		/// <param name="p">Grid coordinate.</param>
		/// <param name="d">Direction.</param>
		/// <param name="e">Entity to set.</param>
		public void SetAt( Vector2 p, Direction d, Entity e )
		{
			SetAt( GridRelative( p, d ), e );
		}

		/// <summary>
		/// EraseAt removes the entity at the given grid cooridnate.
		/// Does nothing if no entity is there.
		/// </summary>
		/// <param name="p">Grid cooridnate.</param>
		/// <param name="d">Direction.</param>
		public void Eraseat( Vector2 p, Direction d )
		{
			EraseAt( GridRelative( p, d ) );
		}

		#endregion




		private Dictionary<Vector2, Entity> entities = new Dictionary<Vector2, Entity>( );

	}

}
