﻿using Sys = System;
using System.Collections.Generic;

using Godot;




namespace Factorial.Core.System
{

	/// <summary>
	/// FileSystem holds utility functions related to filesystem access.
	/// </summary>
	public sealed class FileSystem
	{

		#region CHECKS

		/// <summary>
		/// HasDirectory checks if the given path is a directory. If nothing
		/// exists at the given path, the function returns false.
		/// </summary>
		/// <param name="path">Path to check.</param>
		/// <returns>True if it's a directory.</returns>
		public static bool HasDirectory( string path )
		{
			return _dir.DirExists( path );
		}

		/// <summary>
		/// HasFile checks if the given path is a file. If nothing exists
		/// at the given path, the function returns false.
		/// </summary>
		/// <param name="path">Path to check.</param>
		/// <returns>True if it's a file.</returns>
		public static bool HasFile( string path )
		{
			return _dir.FileExists( path );
		}

		#endregion

		#region ENUMERATIONS

		/// <summary>
		/// ListDirectory yields all files/directories inside the given 
		/// directory.
		/// </summary>
		/// <param name="root">Directory to list.</param>
		/// <returns>Enumerable</returns>
		public static IEnumerable<string> ListDirectory( string root )
		{
			if( !HasDirectory( root ) )
				throw new Sys.IO.FileNotFoundException( "not a directory", root );


			Directory directory = new Directory( );

			if( directory.Open( root ) != OK )
				throw new Sys.IO.FileLoadException( "can't open directory", root );
			if( directory.ListDirBegin( true, true ) != OK )
				throw new Sys.IO.FileLoadException( "can't list directory", root );


			string next = directory.GetNext( );

			while( !next.Empty() )
			{
				if( next.BeginsWith( "." ) )
				{
					next = directory.GetNext( );
					continue;
				}

				yield return next;
			}
		}

		/// <summary>
		/// AllFilePath returns all file path with the given extension. Recursive.
		/// </summary>
		/// <param name="root">Root directory of the search.</param>
		/// <param name="ext">Extension.</param>
		/// <returns>Enumerable</returns>
		public static IEnumerable<string> AllFilePath( string root, string ext )
		{
			List<string> directories = new List<string>( );


			foreach( string name in ListDirectory( root ) )
			{
				string path = root.PlusFile( name );

				if( HasFile( path ) && path.EndsWith( ext ) )
					yield return path;

				if( HasDirectory( path ) )
					directories.Add( path );
			}


			foreach( string dirpath in directories )
				foreach( string path in AllFilePath( dirpath, ext ) )
					yield return path;
		}

		#endregion




		private static Directory _dir = new Directory( );

	}

}
