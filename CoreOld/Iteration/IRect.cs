﻿using Godot;
using System.Collections.Generic;


namespace Factorial.Core.Iteration
{
	abstract class IRect
	{

		public static IEnumerable<Vector2> WholeSteps( Vector2 s, Vector2 e )
		{
			for( float x = s.x; x < e.x; x += 1 )
				for( float y = s.y; y < e.y; y += 1 )
					yield return new Vector2( x, y );
		}

		public static IEnumerable<Vector2> RectSteps( Vector2 tl, Vector2 s )
		{
			return WholeSteps( tl, tl + s );
		}

	}
}
