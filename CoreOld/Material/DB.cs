﻿using Godot;
using System.Collections.Generic;



namespace Factorial.Core.Material
{
	public class DB : Node
	{
		public static DB I { get; private set; }



		private Dictionary<string, Material> list;



		public DB( )
		{
			I = this;
		}



		public override void _Ready( )
		{
			list = new Dictionary<string, Material>( );

			tier0_materials( );
		}




		public bool Has( string n )
		{
			return list.ContainsKey( n );
		}

		public Material GetMaterial( string n )
		{
			if( !Has( n ) )
				return null;

			return list[ n ];
		}




		public void Add( Material m )
		{
			if( Has( m.Name ) )
				return;

			list[ m.Name ] = m;
		}




		private void tier0_materials( )
		{
			Add( new Material( "void", Color.ColorN( "purple" ) ) );
		}

	}
}
