﻿using Godot;


namespace Factorial.Core.Material
{
	public class Material
	{
		public string Name { get; }
		public Color Color { get; }




		public Material( string n, Color c )
		{
			Name = n;
			Color = c;
		}
	}
}
