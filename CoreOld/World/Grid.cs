using Godot;
using System.Collections.Generic;

using Factorial.Core.Iteration;



namespace Factorial.Core.World
{

	public class Grid : Node2D
	{
		public const int O_UP = 0;
		public const int O_RIGHT = 1;
		public const int O_DOWN = 2;
		public const int O_LEFT = 3;




		[Export]
		public Vector2 CellSize;




		private Dictionary<Vector2, Entity> entities;




		public override void _Ready( )
		{
			entities = new Dictionary<Vector2, Entity>( );
		}




		#region SPACE

		public Vector2 GridToWorld( Vector2 p )
		{
			return ToGlobal( p * CellSize );
		}

		public Vector2 WorldToGrid( Vector2 p )
		{
			return ( ToLocal( p ) / CellSize ).Floor( );
		}

		public Vector2 GSizeToWSize( Vector2 s )
		{
			return (s * CellSize).Floor( );
		}

		public Vector2 WSizeToGSize( Vector2 s )
		{
			return (s / CellSize).Ceil( );
		}

		public Vector2 Snap( Vector2 p )
		{
			return GridToWorld( WorldToGrid( p ) );
		}




		public Vector2 Relative( Vector2 p, int o )
		{
			switch( o )
			{
				case O_UP:
					return p + new Vector2( 0, -1 );
				case O_RIGHT:
					return p + new Vector2( 1, 0 );
				case O_DOWN:
					return p + new Vector2( 0, 1 );
				case O_LEFT:
					return p + new Vector2( -1, 0 );
				default:
					return new Vector2( );
			}
		}

		#endregion

		#region CELL LEVEL - GRID SPACE

		public bool HasAt( Vector2 p )
		{
			return entities.ContainsKey( p.Floor( ) );
		}

		public bool HasRelativeAt( Vector2 p, int o )
		{
			return HasAt( Relative( p, o ) );
		}

		public Entity GetAt( Vector2 p )
		{
			if( !HasAt( p ) )
				return null;

			return entities[ p.Floor( ) ];
		}

		public Entity GetRelativeAt( Vector2 p, int o )
		{
			return GetAt( Relative( p, o ) );
		}

		public void SetAt( Vector2 p, Entity e )
		{
			entities[ p.Floor( ) ] = e;
		}

		public void EraseAt( Vector2 p )
		{
			if( HasAt( p ) )
				entities.Remove( p.Floor( ) );
		}

		public Entity[] GetNeighbours( Vector2 p )
		{
			Vector2[ ] l = new Vector2[ 4 ]
			{
				new Vector2( p + new Vector2(  0, -1 ) ),
				new Vector2( p + new Vector2(  1,  0 ) ),
				new Vector2( p + new Vector2(  0,  1 ) ),
				new Vector2( p + new Vector2( -1,  0 ) )
			};

			List<Entity> res = new List<Entity>( );

			foreach( Vector2 np in l )
				if( HasAt( np ) )
					res.Add( GetAt( np ) );

			return res.ToArray( );
		}

		#endregion

		#region CELL LEVEL - WORLD SPACE

		public bool HasAtW( Vector2 p )
		{
			return HasAt( WorldToGrid( p ) );
		}

		public bool HasRelativeAtW( Vector2 p, int o )
		{
			return HasRelativeAt( WorldToGrid( p ), o );
		}

		public Entity GetAtW( Vector2 p )
		{
			return GetAt( WorldToGrid( p ) );
		}

		public Entity GetRelativeAtW( Vector2 p, int o )
		{
			return GetRelativeAt( WorldToGrid( p ), o );
		}

		public void SetAtW( Vector2 p, Entity e )
		{
			SetAt( WorldToGrid( p ), e );
		}

		public void EraseAtW( Vector2 p )
		{
			EraseAt( WorldToGrid( p ) );
		}

		public Entity[] GetNeighboursW( Vector2 p )
		{
			return GetNeighbours( WorldToGrid( p ) );
		}

		#endregion

		#region AREA LEVEL - GRID SPACE

		public Region Region( Vector2 tl, Vector2 s )
		{
			return new Region( this, tl, s );
		}

		public bool HasIn( Vector2 tl, Vector2 s )
		{
			return Region( tl, s ).Has( );
		}

		public Entity[] GetIn( Vector2 tl, Vector2 s )
		{
			return Region( tl, s ).GetEntities( );
		}

		public void SetIn( Vector2 tl, Vector2 s, Entity e )
		{
			Region( tl, s ).Set( e );
		}

		public void EraseIn( Vector2 tl, Vector2 s )
		{
			Region( tl, s ).Erase( );
		}

		#endregion

		#region AREA LEVEL - WORLD SPACE

		public Region RegionW( Vector2 tl, Vector2 s )
		{
			return new Region( this, WorldToGrid( tl ), WSizeToGSize( s ) );
		}

		public bool HasInW( Vector2 tl, Vector2 s )
		{
			return RegionW( tl, s ).Has( );
		}

		public Entity[] GetInW( Vector2 tl, Vector2 s )
		{
			return RegionW( tl, s ).GetEntities( );
		}

		public void SetInW( Vector2 tl, Vector2 s, Entity e )
		{
			RegionW( tl, s ).Set( e );
		}

		public void EraseInW( Vector2 tl, Vector2 s )
		{
			RegionW( tl, s ).Erase( );
		}

		#endregion

		#region ENTITY LEVEL - GRID SPACE

		public bool HasSpace( Vector2 p, Entity e )
		{
			return !HasIn( p, e.Size( ) );
		}

		public void Place( Vector2 p, Entity e )
		{
			foreach( Vector2 ip in IRect.RectSteps( p, e.Size() ) )
			{
				if( HasAt( ip ) )
					Remove( ip );

				SetAt( ip, e );
			}

			e.Position = p * CellSize;
			AddChild( e );
		}

		public Entity Remove( Vector2 p )
		{
			if( !HasAt( p ) )
				return null;

			Entity e = GetAt( p );
			Vector2 ep = ( e.Position / CellSize ).Floor( );

			EraseIn( ep, e.Size( ) );
			RemoveChild( e );

			return e;
		}

		public void UpdateNeighbour( Vector2 p )
		{
			foreach( Entity e in GetNeighbours( p ) )
				e.OnNeighbourUpdated( );
		}

		#endregion

		#region ENTITY LEVEL - WORLD SPACE

		public bool HasSpaceW( Vector2 p, Entity e )
		{
			return HasIn( WorldToGrid( p ), e.Size( ) );
		}

		public void PlaceW( Vector2 p, Entity e )
		{
			Place( WorldToGrid( p ), e );
		}

		public void RemoveW( Vector2 p )
		{
			Remove( WorldToGrid( p ) );
		}

		public void UpdateNeighbourW( Vector2 p )
		{
			UpdateNeighbour( WorldToGrid( p ) );
		}

		#endregion

		#region PORT LEVEL - GRID SPACE

		public int GetSlotAt( Vector2 p, int o )
		{
			if( !HasRelativeAt( p, o ) )
				return -1;

			Entity e = GetRelativeAt( p, o );

			if( o == O_RIGHT )
				p.x += 1;
			if( o == O_DOWN )
				p.y += 1;

			Vector2 ep = WorldToGrid( e.Position );

			int i = e.EdgeSlots.Index( p - ep );

			if( o == O_RIGHT && i == 0 )
				return e.EdgeSlots.ToLocal( e.EdgeSlots.Count - 1 );

			return e.EdgeSlots.ToLocal( i );
		}

		public bool HasPortAt( Vector2 p, int o )
		{
			if( !HasRelativeAt( p, o ) )
				return false;

			Entity e = GetRelativeAt( p, o );
			int sidx = GetSlotAt( p, o );

			return e.Ports.Has( sidx );
		}

		public Port GetPortAt( Vector2 p, int o )
		{
			if( !HasPortAt( p, o ) )
				return null;

			return GetRelativeAt( p, o ).Ports.GetPort( GetSlotAt( p, o ) );
		}

		#endregion

		#region PORT LEVEL - WORLD SPACE
		#endregion
	}

}
