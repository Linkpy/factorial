﻿
namespace Factorial.Core.World
{
	public class Port
	{
		public const int IN = 0;
		public const int OUT = 1;




		public int Slot { get; }
		public int Kind { get; }
		public string Material { get; }




		public Port( int s, int k, string m )
		{
			Slot = s;
			Kind = k;
			Material = m;
		}
	}
}
