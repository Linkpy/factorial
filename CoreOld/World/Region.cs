﻿using Godot;
using System.Collections.Generic;

using Factorial.Core.Iteration;


namespace Factorial.Core.World
{
	public class Region
	{
		public Vector2 TopLeft { get; }
		public Vector2 Size { get; }

		private Grid grid;




		public Region( Grid g, Vector2 tl, Vector2 s )
		{
			TopLeft = tl.Floor( );
			Size = s.Ceil( );

			grid = g;
		}




		public bool Has( )
		{
			foreach( Vector2 p in IRect.RectSteps( TopLeft, Size ) )
				if( grid.HasAt( p ) )
					return true;

			return false;
		}

		public void Set( Entity e )
		{
			foreach( Vector2 p in IRect.RectSteps( TopLeft, Size ) )
				grid.SetAt( p, e );
		}

		public void Erase( )
		{
			foreach( Vector2 p in IRect.RectSteps( TopLeft, Size ) )
				grid.EraseAt( p );
		}




		public Entity[] GetEntities( )
		{
			List<Entity> res = new List<Entity>( );

			foreach( Vector2 p in IRect.RectSteps( TopLeft, Size ) )
			{
				if( !grid.HasAt( p ) )
					continue;

				Entity e = grid.GetAt( p );

				if( !res.Contains( e ) )
					res.Add( e );
			}

			return res.ToArray( );
		}
	}
}
