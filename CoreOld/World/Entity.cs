﻿using Godot;

using Factorial.Core.World.Modules;



namespace Factorial.Core.World
{
	public class Entity : Node2D
	{

		[Export]
		public string				Kind;
		[Export]
		public string				Identifier;
		[Export]
		public string				Category;

		[Export]
		public int					Width = 1;
		[Export]
		public int					Height = 1;

		[Export(PropertyHint.MultilineText)]
		public string				PortsDeclarationCode = "";




		public Grid					Grid;

		public OrientationModule	Orientation;
		public EdgeSlotsModule		EdgeSlots;
		public PortsModule          Ports;
		public VisualModule         Visual;




		public Entity( ) : base( )
		{
			Orientation = new OrientationModule( this );
			EdgeSlots = new EdgeSlotsModule( this );
			Ports = new PortsModule( this );
			Visual = new VisualModule( this );

			Orientation.Connect( "updated", this, "onOrientationUpdate" );
		}




		public virtual Vector2 Size( )
		{
			return new Vector2( Width, Height );
		}




		public virtual void CopyTo( Entity e )
		{
			Orientation.CopyTo( e.Orientation );
		}




		public override void _Ready( )
		{
			if( !(GetParent( ) is Grid) )
				return;

			Grid = GetParent<Grid>( );

			Ports.Parse( PortsDeclarationCode );

			OnNeighbourUpdated( );
		}

		public override void _Draw( )
		{
			Visual.Draw( );
		}




		public virtual void onOrientationUpdate( )
		{
			Sprite s = GetNode<Sprite>( "sprite" );

			switch( Orientation.Orientation )
			{
				case Grid.O_UP:
					s.RotationDegrees = 0;
					break;
				case Grid.O_RIGHT:
					s.RotationDegrees = 90;
					break;
				case Grid.O_DOWN:
					s.RotationDegrees = 180;
					break;
				case Grid.O_LEFT:
					s.RotationDegrees = 270;
					break;
			}
		}




		public virtual void OnNeighbourUpdated( )
		{
		}

	}
}
