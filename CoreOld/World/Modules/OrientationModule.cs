using Godot;

using Factorial.Core.World;



namespace Factorial.Core.World.Modules
{
	public class OrientationModule : Module
	{
		[Signal]
		public delegate void updated( );



		private int orientation;

		public int Orientation {
			get {
				return orientation;
			}
			set {
				orientation = Wrap( value );
				EmitSignal( "updated" );
			}
		}




		public OrientationModule( Entity e ) : base( e )
		{
			orientation = 0;
		}




		public override void CopyTo( Module m )
		{
			if( m is OrientationModule )
				( (OrientationModule) m ).Orientation = orientation;
		}




		public int ToLocal( int o )
		{
			return Wrap( o - orientation );
		}

		public int ToGlobal( int o )
		{
			return Wrap( orientation + o );
		}

		public int Wrap( int o )
		{
			while( o > Grid.O_LEFT )
				o -= 4;
			while( o < 0 )
				o += 4;

			return o;
		}




		public void RotateCW( )
		{
			Orientation += 1;
		}

		public void RotateCCW( )
		{
			Orientation -= 1;
		}
	}
}
