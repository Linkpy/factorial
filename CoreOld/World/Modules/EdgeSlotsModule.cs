﻿using Godot;

using Factorial.Core.World;



namespace Factorial.Core.World.Modules
{
	public class EdgeSlotsModule : Module
	{

		public int Count => ( Entity.Width + Entity.Height ) * 2;




		public EdgeSlotsModule( Entity e ) : base( e )
		{ }




		public int OrientationOffset( )
		{
			switch( Entity.Orientation.Orientation )
			{
				case Grid.O_UP:
					return 0;
				case Grid.O_RIGHT:
					return Entity.Width;
				case Grid.O_DOWN:
					return Entity.Width + Entity.Height;
				case Grid.O_LEFT:
					return 2 * Entity.Width + Entity.Height;
			}

			return -1;
		}

		public int ToLocal( int idx )
		{
			return Wrap( idx - OrientationOffset( ) );
		}

		public int ToGlobal( int idx )
		{
			return Wrap( OrientationOffset( ) + idx );
		}

		public int Wrap( int idx )
		{
			int c = Count;

			while( idx >= c )
				idx -= c;
			while( idx < 0 )
				idx += c;

			return idx;
		}




		public int Orientation( int idx )
		{
			idx = Wrap( idx );

			if( idx < Entity.Width )
				return Grid.O_UP;
			else if( idx < Entity.Width + Entity.Height )
				return Grid.O_RIGHT;
			else if( idx < 2 * Entity.Width + Entity.Height )
				return Grid.O_DOWN;
			else
				return Grid.O_LEFT;
		}

		public Vector2 Position( int idx )
		{
			idx = Wrap( idx );

			if( idx < Entity.Width )
				return new Vector2( idx, 0 );
			else if( idx < Entity.Width + Entity.Height )
				return new Vector2( Entity.Width, idx - Entity.Width );
			else if( idx < 2 * Entity.Width + Entity.Height )
				return new Vector2( idx - Entity.Width - Entity.Height, Entity.Height );
			else
				return new Vector2( 0, idx - 2*Entity.Width - Entity.Height );
		}

		public int Index( Vector2 p )
		{
			if( p.x < 0 || p.x > Entity.Width )
				return -1;
			if( p.y < 0 || p.y > Entity.Height )
				return -1;

			if( p.y == 0 )
				return (int) p.x;
			else if( p.x == Entity.Width )
				return Entity.Width + (int) p.y;
			else if( p.y == Entity.Height )
				return Entity.Width + Entity.Height + (int) p.x;
			else if( p.x == 0 )
				return 2 * Entity.Width + Entity.Height + (int) p.y;

			return -1;
		}
	}
}
