﻿using Godot;

using Factorial.Core.Material;


namespace Factorial.Core.World.Modules
{
	public class VisualModule : Module
	{

		public bool ShowSelection
		{
			get => showSelection;
			set {
				showSelection = value;
				Entity.Update( );
			}
		}

		public bool ShowPorts
		{
			get => showPorts;
			set {
				showPorts = value;
				Entity.Update( );
			}
		}




		private bool showSelection;
		private bool showPorts;




		public VisualModule( Entity e ) : base( e )
		{
			showSelection = false;
			showPorts = false;
		}




		public void Draw( )
		{
			if( showSelection )
				drawSelection( );
			if( showPorts )
				drawPorts( );
		}




		private void drawSelection( )
		{
			Vector2[ ] points = new Vector2[ 5 ]
			{
				new Vector2( 0, 0 ),
				new Vector2( Entity.Width, 0 ) * Entity.Grid.CellSize,
				new Vector2( Entity.Width, Entity.Height ) * Entity.Grid.CellSize,
				new Vector2( 0, Entity.Height ) * Entity.Grid.CellSize,
				new Vector2( 0, 0 )
			};

			Entity.DrawPolyline( points, new Color( 0, 1, 0 ), 5 );
		}

		private void drawPorts( )
		{
			foreach( Port port in Entity.Ports.Enumerate( ) )
			{
				int gidx = Entity.EdgeSlots.ToGlobal( port.Slot );

				Vector2 pos = Entity.EdgeSlots.Position( gidx );
				int ori = Entity.EdgeSlots.Orientation( gidx );
				Material.Material m = Material.DB.I.GetMaterial( port.Material );

				switch( port.Kind )
				{
					case Port.IN:
						drawInPort( pos, ori, m.Color );
						break;
					case Port.OUT:
						drawOutPort( pos, ori, m.Color );
						break;
				}
			}
		}

		private void drawInPort( Vector2 p, int o, Color c )
		{
			Vector2[ ] poly = new Vector2[ 3 ];

			switch( o )
			{
				case Grid.O_UP:
					poly[ 0 ] = p + new Vector2( 0.4f, -0.15f );
					poly[ 1 ] = p + new Vector2( 0.5f, 0 );
					poly[ 2 ] = p + new Vector2( 0.6f, -0.15f );
					break;
				case Grid.O_RIGHT:
					poly[ 0 ] = p + new Vector2( 0.15f, 0.4f );
					poly[ 1 ] = p + new Vector2( 0, 0.5f );
					poly[ 2 ] = p + new Vector2( 0.15f, 0.6f );
					break;
				case Grid.O_DOWN:
					poly[ 0 ] = p + new Vector2( 0.4f, 0.15f );
					poly[ 1 ] = p + new Vector2( 0.5f, 0 );
					poly[ 2 ] = p + new Vector2( 0.6f, 0.15f );
					break;
				case Grid.O_LEFT:
					poly[ 0 ] = p + new Vector2( -0.15f, 0.4f );
					poly[ 1 ] = p + new Vector2( 0, 0.5f );
					poly[ 2 ] = p + new Vector2( -0.15f, 0.6f );
					break;
			}

			for( int i = 0; i < 3; i++ )
				poly[ i ] *= Entity.Grid.CellSize;

			Entity.DrawPolygon( poly, new Color[ 3 ] { c, c, c } );
		}

		private void drawOutPort( Vector2 p, int o, Color c )
		{
			Vector2[ ] poly = new Vector2[ 3 ];

			switch( o )
			{
				case Grid.O_UP:
					poly[ 0 ] = p + new Vector2( 0.4f, 0 );
					poly[ 1 ] = p + new Vector2( 0.5f, -0.15f );
					poly[ 2 ] = p + new Vector2( 0.6f, 0 );
					break;
				case Grid.O_RIGHT:
					poly[ 0 ] = p + new Vector2( 0, 0.4f );
					poly[ 1 ] = p + new Vector2( 0.15f, 0.5f );
					poly[ 2 ] = p + new Vector2( 0, 0.6f );
					break;
				case Grid.O_DOWN:
					poly[ 0 ] = p + new Vector2( 0.4f, 0 );
					poly[ 1 ] = p + new Vector2( 0.5f, 0.15f );
					poly[ 2 ] = p + new Vector2( 0.6f, 0 );
					break;
				case Grid.O_LEFT:
					poly[ 0 ] = p + new Vector2( 0, 0.4f );
					poly[ 1 ] = p + new Vector2( -0.15f, 0.5f );
					poly[ 2 ] = p + new Vector2( 0, 0.6f );
					break;
			}

			for( int i = 0; i < 3; i++ )
				poly[ i ] *= Entity.Grid.CellSize;

			Entity.DrawPolygon( poly, new Color[ 3 ] { c, c, c } );
		}
	}
}
