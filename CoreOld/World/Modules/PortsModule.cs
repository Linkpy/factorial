﻿using Godot;
using System.Collections.Generic;

using Factorial.Core.World;



namespace Factorial.Core.World.Modules
{
	public class PortsModule : Module
	{
		private Dictionary<int, Port> ports;

		public int Count => ports.Count;




		public PortsModule( Entity e ) : base( e )
		{
			ports = new Dictionary<int, Port>( );
		}



		
		public bool Has( int idx )
		{
			return ports.ContainsKey( Entity.EdgeSlots.Wrap( idx ) );
		}

		public Port GetPort( int idx )
		{
			idx = Entity.EdgeSlots.Wrap( idx );

			if( !ports.ContainsKey( idx ) )
				return null;

			return ports[ idx ];
		}

		public IEnumerable<Port> Enumerate( )
		{
			return ports.Values;
		}




		public void Add( int idx, int k, string mat )
		{
			if( Has( idx ) )
				return;

			idx = Entity.EdgeSlots.Wrap( idx );
			ports[ idx ] = new Port( idx, k, mat );
		}



		
		public bool IsInput( int idx )
		{
			Port p = GetPort( idx );

			if( p == null )
				return false;

			return p.Kind == Port.IN;
		}

		public bool IsOutput( int idx )
		{
			Port p = GetPort( idx );

			if( p == null )
				return false;

			return p.Kind == Port.OUT;
		}




		public void Parse( string s )
		{
			foreach( string l in s.Split( "\n" ) )
				parseLine( l );
		}

		private void parseLine( string l )
		{
			string[ ] parts = l.Split( " " );

			int slot = parts[ 0 ].ToInt( );
			int kind = -1;
			string mat = parts[ 2 ];

			switch( parts[ 1 ] )
			{
				case "IN":
					kind = Port.IN;
					break;
				case "OUT":
					kind = Port.OUT;
					break;
			}

			Add( slot, kind, mat );
		}
	}
}
