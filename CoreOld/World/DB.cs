﻿using Godot;
using System.Collections.Generic;

using Factorial.Core;



namespace Factorial.Core.World
{
	public class DB : Node
	{
		public static DB I { get; private set; }




		private Dictionary<string, Entity> list;




		public DB( )
		{
			I = this;
		}




		public override void _Ready( )
		{
			list = new Dictionary<string, Entity>( );
			
			GD.Print( "> Searching for entities..." );
			string[ ] ents = FS.AllFiles( "res://", ".ent.tscn" );
			GD.Print( string.Format( "> {0} entities found, loading...", ents.Length ) );

			foreach( string p in ents )
			{
				Entity e = GD.Load<PackedScene>( p ).Instance( ) as Entity;
				Add( e );
			}
		}




		public bool Has( string id )
		{
			return list.ContainsKey( id );
		}

		public Entity GetEntity( string id )
		{
			if( !Has( id ) )
				return null;

			return list[ id ];
		}

		public void Add( Entity e )
		{
			if( Has( e.Identifier ) )
			{
				GD.PrintErr( string.Format( "- Entity '{0}' already exists.", e.Identifier ) );
				return;
			}

			list[ e.Identifier ] = e;
			GD.Print( string.Format( "+ Entity '{0}' declared.", e.Identifier ) );
		}




		public IEnumerable<string> Kind( string k )
		{
			foreach( Entity e in list.Values )
				if( e.Kind == k )
					yield return e.Identifier;
		}

		public IEnumerable<string> Categories( string k )
		{
			List<string> yielded = new List<string>( );

			foreach( Entity e in list.Values )
			{
				if( e.Kind == k && !yielded.Contains( e.Category ) )
				{
					yielded.Add( e.Category );
					yield return e.Category;
				}
			}
		}

		public IEnumerable<string> KindAndCategory( string k, string c )
		{
			foreach( Entity e in list.Values )
				if( e.Kind == k && e.Category == c )
					yield return e.Identifier;
		}

	}
}
