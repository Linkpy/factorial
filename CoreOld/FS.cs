﻿using Godot;
using System.Collections.Generic;



namespace Factorial.Core
{

	public class FS
	{

		public static string[] AllFiles( string root, string ext )
		{
			Directory dir = new Directory( );

			if( !dir.DirExists( root ) )
				return null;

			dir.Open( root );
			dir.ListDirBegin( true, true );

			List<string> res = new List<string>( );
			List<string> dirs = new List<string>( );
			string next = dir.GetNext( );

			while( !next.Empty() )
			{
				if( next.BeginsWith( "." ) )
				{
					next = dir.GetNext( );
					continue;
				}

				string fpath = root.PlusFile( next );

				if( dir.FileExists( fpath ) && fpath.EndsWith( ext ) )
					res.Add( fpath );

				if( dir.DirExists( fpath ) )
					dirs.Add( fpath );

				next = dir.GetNext( );
			}

			foreach( string d in dirs )
			{
				string[ ] r = AllFiles( d, ext );

				foreach( string p in r )
					res.Add( p );
			}

			return res.ToArray( );
		}

	}
}
