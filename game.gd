extends Node2D



const Ghost = preload( "ui/ghost/ghost.tscn" )




onready var grid = $grid
onready var ingame = $ui/ingame




var ghost = null
var hovered_ent = null




func _input( event: InputEvent ) -> void:
	if ingame.gui_in_use:
		return
	
	if event.is_action_pressed( "lclick" ):
		if ghost == null:
			return
		
		_place_machine( )
	
	if event.is_action_pressed( "rclick" ):
		if ghost == null:
			_remove_machine( )
		else:
			_cancel_machine_placement( )
	
	if event.is_action_pressed( "rotate" ):
		if ghost != null:
			_rotate_ghost_machine( )
	
	if event.is_action_pressed( "show_ports" ):
		grid.show_ports = not grid.show_ports
	
	if event is InputEventMouseMotion:
		_update_machine_rect( )




func _place_machine( ) -> void:
	var p = grid.world_to_grid( get_global_mouse_position( ) )
	
	if grid.has_space( ghost.machine, p ):
		var e = ghost.machine.duplicate()
		
		e.orientation = ghost.machine.orientation
		
		grid.place( e, p )

func _remove_machine( ) -> void:
	var p = grid.world_to_grid( get_global_mouse_position( ) )
	
	if grid.has_at( p ):
		grid.remove( p ).queue_free( )
		hovered_ent = null

func _cancel_machine_placement( ) -> void:
	ghost.queue_free( )
	ghost = null

func _rotate_ghost_machine( ) -> void:
	ghost.machine.rotate_clockwise( )

func _update_machine_rect( ) -> void:
	var p = grid.world_to_grid( get_global_mouse_position( ) )
	
	if grid.has_at( p ):
		if hovered_ent != null:
			hovered_ent.show_selection_rect = false
		
		hovered_ent = grid.get_at( p )
		hovered_ent.show_selection_rect = true
	
	elif hovered_ent != null:
		hovered_ent.show_selection_rect = false
		hovered_ent = null




func _on_ingame_machine_selected(e) -> void:
	if ghost != null:
		ghost.queue_free( )
		ghost = null
	
	ghost = Ghost.instance( )
	
	ghost.add_child( e )
	add_child( ghost )



# TODO:
# - [DONE] Clean up a bit what's already present
#	- [DONE] Ghost should emit signal to place and cancel
#	- [DONE] Makes Category and Machine emits "selected" only with the entity ID
#	- [DONE] Normalize machine orientations
#	- [DONE] Machine-relative orientation (for ports)
#	- [DONE] Ghost should check if the machine can be placed
# - [DONE] Implement removing machines
# - [DONE] CLEAN UP
# - [DONE] Implement camera controls
# - [DONE] Implement hiding the machine selection UI
# - [DONE] Implement the machine search UI
# - [DONE] Implement machine port spots
# - [DONE] Move machine-related visuals to the entity class
# - [DONE] Implement material database
# - [DONE] Implement machine port declarations
# - CLEAN UP
# - Implement machine port connections
# - Implement material flow
# - Implement grid chunking
# - Implement inventory