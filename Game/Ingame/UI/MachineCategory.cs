﻿using Godot;
using GC = Godot.Collections;



namespace Factorial.Game.Ingame.UI
{
	public class MachineCategory : VBoxContainer
	{

		[Signal]
		public delegate void selected( string id );



		private Label category;
		private Button fold;
		private MarginContainer margin;
		private VBoxContainer items;




		public override void _Ready( )
		{
			category = GetNode<Label>( "header/name" );
			fold = GetNode<Button>( "header/fold" );
			margin = GetNode<MarginContainer>( "margin" );
			items = GetNode<VBoxContainer>( "margin/items" );
		}




		public void Setup( string c, string[] vals )
		{
			category.Text = c;

			foreach( string val in vals )
			{
				GC.Array binds = new GC.Array( );
				binds.Add( val );

				Button b = new Button( );
				b.Text = val;
				b.Connect( "pressed", this, "selection", binds );

				items.AddChild( b );
			}
		}




		public void selection( string n )
		{
			EmitSignal( "selected", n );
		}

		public void onFoldPressed( )
		{
			margin.Visible = !margin.Visible;
			fold.Text = margin.Visible ? "-" : "+";
		}

	}
}
