﻿using Godot;

using Factorial.Core.World;




namespace Factorial.Game.Ingame.UI
{
	public class MachineGhost : Node2D
	{
		public Grid Grid;
		public Entity Machine;




		public MachineGhost( Entity m )
		{
			Machine = m;
		}



		public override void _Ready( )
		{
			Grid = ((Game)GetParent()).Grid;
			AddChild( Machine );
		}




		public override void _Process( float delta )
		{
			Vector2 mpos = GetGlobalMousePosition( );
			Position = Grid.Snap( mpos );
		}
	}
}
