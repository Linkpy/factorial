﻿using Godot;
using GC = Godot.Collections;
using System.Linq;

using Factorial.Core.World;



namespace Factorial.Game.Ingame.UI
{
	public class Machines : Panel
	{
		public static PackedScene Category = GD.Load<PackedScene>( "res://ui/ingame/sub/category/category.tscn" );




		[Signal]
		public delegate void using_gui( bool v );
		[Signal]
		public delegate void selected( string id );




		private VBoxContainer categories;
		private VBoxContainer searchResults;
		private AnimationPlayer anim;

		private string[] machines;




		public override void _Ready( )
		{
			categories = GetNode<VBoxContainer>( "vbox/categories" );
			searchResults = GetNode<VBoxContainer>( "vbox/search_results" );
			anim = GetNode<AnimationPlayer>( "anim" );

			machines = DB.I.Kind( "machine" ).ToArray( );

			string[ ] cats = DB.I.Categories( "machine" ).ToArray( );

			foreach( string cat in cats )
			{
				string[ ] vals = DB.I.KindAndCategory( "machine", cat ).ToArray( );

				MachineCategory c = Category.Instance( ) as MachineCategory;
				categories.AddChild( c );

				c.Connect( "selected", this, "onSelected" );
				c.Setup( cat, vals );
			}
		}

		public override void _Input( InputEvent ev )
		{
			if( !(ev is InputEventMouseMotion) )
				return;

			Vector2 mp = GetLocalMousePosition( );

			if( mp.x >= -48 )
			{
				anim.Play( "open" );
				EmitSignal( "using_gui", true );
			}
			else
			{
				anim.Play( "close" );
				EmitSignal( "using_gui", false );
			}
		}




		public void onSelected( string id )
		{
			EmitSignal( "selected", id );
		}




		public void onSearchTextChanged( string ntxt )
		{
			foreach( Node c in searchResults.GetChildren( ) )
				c.QueueFree( );

			if( ntxt.Empty( ) )
			{
				searchResults.Hide( );
				categories.Show( );
			}
			else
			{
				searchResults.Show( );
				categories.Hide( );

				int found = 0;

				foreach( string m in machines )
				{
					if( m.Find( ntxt ) == -1 )
						continue;

					Button b = new Button( );
					GC.Array binds = new GC.Array( );

					binds.Add( m );

					b.Text = m;
					b.Connect( "pressed", this, "onSelected", binds );

					searchResults.AddChild( b );
					found += 1;
				}

				if( found == 0 )
				{
					Label l = new Label( );
					l.Text = "No result";
					searchResults.AddChild( l );
				}
			}
		}
	}

}
