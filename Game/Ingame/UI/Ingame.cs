using Godot;

using Factorial.Core.World;



namespace Factorial.Game.Ingame.UI
{
	public class Ingame : Control
	{

		[Signal]
		public delegate void machine_selected( Entity e );

		[Signal]
		public delegate void gui_in_use( bool v );




		public void onMachineSelected( string id )
		{
			Entity e = DB.I.GetEntity( id ).Duplicate( ) as Entity;
			EmitSignal( "machine_selected", e );
		}

		public void onUsingGui( bool b )
		{
			EmitSignal( "gui_in_use", b );
		}


	}
}
