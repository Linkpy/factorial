﻿using Godot;

using Factorial.Core.World;
using Factorial.Game.Ingame.UI;


namespace Factorial.Game.Ingame
{
	public class Game : Node2D
	{

		public Grid Grid { get; private set; }
		public UI.Ingame Ingame { get; private set; }
		public Camera Camera { get; private set; }


		private MachineGhost ghost;
		private Entity hoveredEntity;

		private bool guiInUse = false;
		private bool showPorts = false;




		public override void _Ready( )
		{
			Grid = GetNode<Grid>( "grid" );
			Camera = GetNode<Camera>( "camera" );
			Ingame = GetNode<UI.Ingame>( "ui/ingame" );
		}

		public override void _Input( InputEvent ev )
		{
			if( guiInUse )
				return;

			if( ev.IsActionPressed( "lclick" ) )
			{
				if( !IsInstanceValid( ghost ) )
					return;

				placeMachine( );
			}

			if( ev.IsActionPressed( "rclick" ) )
			{
				if( !IsInstanceValid( ghost ) )
					removeMachine( );
				else
					cancelMachinePlacement( );
			}

			if( ev.IsActionPressed( "rotate" ) )
				if( IsInstanceValid( ghost ) )
					rotateGhostMachine( );

			if( ev.IsActionPressed( "show_ports" ) )
				showMachinePorts( );

			if( ev is InputEventMouseMotion )
				updateMachineRect( );
		}




		private void placeMachine( )
		{
			Vector2 p = Grid.WorldToGrid( GetGlobalMousePosition( ) );

			if( Grid.HasSpace( p, ghost.Machine ) )
			{
				Entity e = (Entity) ghost.Machine.Duplicate( );

				e.Visual.ShowPorts = showPorts;
				ghost.Machine.CopyTo( e );

				Grid.Place( p, e );
				Grid.UpdateNeighbour( p );
			}
		}

		private void removeMachine( )
		{
			Vector2 p = Grid.WorldToGrid( GetGlobalMousePosition( ) );

			if( Grid.HasAt( p ) )
			{
				Grid.Remove( p ).QueueFree( );
				hoveredEntity = null;
				Grid.UpdateNeighbour( p );
			}
		}

		private void cancelMachinePlacement( )
		{
			ghost.QueueFree( );
			ghost = null;
		}

		private void rotateGhostMachine( )
		{
			ghost.Machine.Orientation.RotateCW( );
		}

		private void showMachinePorts( )
		{
			showPorts = !showPorts;

			foreach( Node ne in Grid.GetChildren() )
			{
				Entity e = ne as Entity;

				if( e != null )
					e.Visual.ShowPorts = showPorts;
			}
		}

		private void updateMachineRect( )
		{
			Vector2 p = Grid.WorldToGrid( GetGlobalMousePosition( ) );

			if( Grid.HasAt( p ) )
			{
				if( hoveredEntity != null )
					hoveredEntity.Visual.ShowSelection = false;

				hoveredEntity = Grid.GetAt( p );
				hoveredEntity.Visual.ShowSelection = true;
			}
			else if( hoveredEntity != null )
			{
				hoveredEntity.Visual.ShowSelection = false;
				hoveredEntity = null;
			}
		}




		private void onMachineSelected( Entity e )
		{
			if( IsInstanceValid( ghost ) )
				ghost.QueueFree( );
			
			ghost = new MachineGhost( e );

			AddChild( ghost );
		}

		private void onGuiInUse( bool v )
		{
			guiInUse = v;
			Camera.EnableInput = !v;
		}

	}
}
