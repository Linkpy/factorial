﻿using Godot;



namespace Factorial.Game.Ingame
{
	public class Camera : Camera2D
	{
		[Export]
		public float Speed = 600;




		public bool EnableInput = true;




		public override void _Process( float delta )
		{
			if( !EnableInput )
				return;

			Position += InputVector( ) * Speed * delta;
		}




		public Vector2 InputVector( )
		{
			Vector2 v = new Vector2( );

			if( Input.IsActionPressed( "camera_left" ) )
				v.x -= 1;
			if( Input.IsActionPressed( "camera_right" ) )
				v.x += 1;
			if( Input.IsActionPressed( "camera_up" ) )
				v.y -= 1;
			if( Input.IsActionPressed( "camera_down" ) )
				v.y += 1;

			return v.Normalized( );
		}
	}
}
