﻿using Godot;

using Factorial.Core.World;



namespace Factorial.Game.Content.Transports
{
	public class FlowPipe : Entity
	{
		public const int TOP	= 0x01;
		public const int RIGHT	= 0x02;
		public const int BOTTOM = 0x04;
		public const int LEFT   = 0x08;




		public FlowPipe( ) : base( )
		{ }




		public override void OnNeighbourUpdated( )
		{
			int bf = neighbourBitfield( );
			bitfield_case c = cases[ bf ];

			Sprite s = GetNode<Sprite>( "sprite" );
			s.Frame = c.Frame;
			Orientation.Orientation = c.Orientation;
		}




		private int neighbourBitfield( )
		{
			int res = 0;

			if( connectsTowards( Grid.O_UP ) )
				res |= TOP;
			if( connectsTowards( Grid.O_RIGHT ) )
				res |= RIGHT;
			if( connectsTowards( Grid.O_DOWN ) )
				res |= BOTTOM;
			if( connectsTowards( Grid.O_LEFT ) )
				res |= LEFT;

			return res;
		}

		private bool connectsTowards( int o )
		{
			Vector2 p = Grid.WorldToGrid( Position );

			if( !Grid.HasRelativeAt( p, o ) )
				return false;

			if( Grid.GetRelativeAt( p, o ) is FlowPipe )
				return true;

			return Grid.HasPortAt( p, o );
		}






		private struct bitfield_case
		{
			public int Frame;
			public int Orientation;


			public bitfield_case( int f, int o )
			{
				Frame = f;
				Orientation = o;
			}
		}

		private static bitfield_case[] cases = new bitfield_case[16]
		{
			new bitfield_case( 0, Grid.O_UP ),					// 0000 -
			new bitfield_case( 1, Grid.O_UP ),					// 0001 - TOP
			new bitfield_case( 1, Grid.O_RIGHT ),				// 0010 - RIGHT
			new bitfield_case( 3, Grid.O_UP ),					// 0011 - TOP, RIGHT

			new bitfield_case( 1, Grid.O_DOWN ),				// 0100 - BOTTOM
			new bitfield_case( 2, Grid.O_UP ),					// 0101 - TOP, BOTTOM
			new bitfield_case( 3, Grid.O_RIGHT ),				// 0110 - RIGHT, BOTTOM
			new bitfield_case( 4, Grid.O_UP ),					// 0111 - TOP, RIGHT, BOTTOM

			new bitfield_case( 1, Grid.O_LEFT ),				// 1000 - LEFT
			new bitfield_case( 3, Grid.O_LEFT ),				// 1001 - TOP, LEFT
			new bitfield_case( 2, Grid.O_RIGHT ),				// 1010 - RIGHT, LEFT
			new bitfield_case( 4, Grid.O_LEFT ),				// 1011 - TOP, RIGHT, LEFT

			new bitfield_case( 3, Grid.O_DOWN ),				// 1100 - BOTTOM, LEFT
			new bitfield_case( 4, Grid.O_DOWN ),				// 1101 - TOP, BOTTOM, LEFT
			new bitfield_case( 4, Grid.O_RIGHT ),				// 1110 - RIGHT, BOTTOM, LEFT
			new bitfield_case( 5, Grid.O_UP ),					// 1111 - TOP, RIGHT, BOTTOM, LEFT
		};
	}
}
