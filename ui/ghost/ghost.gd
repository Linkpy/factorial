extends Node2D



onready var grid = $"../grid"
onready var machine = get_child( 0 )




func _process( dt: float ) -> void:
	var mpos = get_global_mouse_position( )
	position = grid.grid_to_world( grid.world_to_grid( mpos ) )
