extends Control




signal machine_selected( e )



var gui_in_use = false



func _on_machines_selected( id ) -> void:
	var e = EntDB.get_ent( id ).duplicate( )
	emit_signal( "machine_selected", e )



func _on_machines_using_gui(v) -> void:
	gui_in_use = v
