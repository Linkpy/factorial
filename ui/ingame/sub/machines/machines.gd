extends Panel


const Category = preload( "../category/category.tscn" )


signal using_gui( v )

signal selected( id )



onready var categories = $vbox/categories
onready var search_results = $vbox/search_results
onready var anim = $anim



var machines: Array




func _ready( ):
	machines = EntDB.kind( "machine" )
	
	var cats = EntDB.categories( "machine" )
	
	for cat in cats:
		var vals = EntDB.kind_and_category( "machine", cat )
		
		var c = Category.instance( )
		categories.add_child( c )
		
		c.connect( "selected", self, "_on_selected" )
		c.setup( cat, vals )




func _input( ev: InputEvent ) -> void:
	
	if not ev is InputEventMouseMotion:
		return
	
	var mp = get_local_mouse_position( )
	
	if mp.x >= -48:
		_on_mouse_entered( )
		emit_signal( "using_gui", true )
	else:
		_on_mouse_exited( )
		emit_signal( "using_gui", false )




func _on_selected( id: String ) -> void:
	emit_signal( "selected", id )




func _on_mouse_entered() -> void:
	anim.play( "open" )

func _on_mouse_exited() -> void:
	anim.play( "close" )




func _on_search_text_changed( new_text: String ) -> void:
	for c in search_results.get_children( ):
		c.queue_free( )
	
	if new_text.empty( ):
		search_results.hide( )
		categories.show( )
	
	else:
		search_results.show( )
		categories.hide( )
		
		var found = 0
		
		for m in machines:
			if m.find( new_text ) == -1:
				continue
			
			var b = Button.new( )
			b.text = m
			b.connect( "pressed", self, "_on_selected", [m] )
			search_results.add_child( b )
			found += 1
		
		if found == 0:
			var l = Label.new( )
			l.text = "No result"
			search_results.add_child( l )
