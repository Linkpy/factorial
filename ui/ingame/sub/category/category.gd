extends VBoxContainer




signal selected( id )




onready var category = $header/name
onready var fold = $header/fold

onready var margin = $margin
onready var items = $margin/items




func setup( c: String, vals: Array ) -> void:
	category.text = c
	
	for val in vals:
		var b = Button.new( )
		b.text = val
		b.connect( "pressed", self, "_selection", [val] )
		items.add_child( b )




func _selection( n: String ) -> void:
	emit_signal( "selected", n )

func _on_fold_pressed() -> void:
	margin.visible = not margin.visible
	fold.text = "-" if margin.visible else "+"
