extends Camera2D




export var speed: float = 600

onready var ingame = $"../ui/ingame"



func _process( dt: float ) -> void:
	position += _input_vector( ) * speed * dt




func _input_vector( ) -> Vector2:
	if ingame.gui_in_use:
		return Vector2( )
	
	var v = Vector2( )
	
	if Input.is_action_pressed( "camera_left" ):
		v.x -= 1
	if Input.is_action_pressed( "camera_right" ):
		v.x += 1
	if Input.is_action_pressed( "camera_up" ):
		v.y -= 1
	if Input.is_action_pressed( "camera_down" ):
		v.y += 1
	
	return v.normalized( )